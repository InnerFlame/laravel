<?php namespace App\Models;


class Post extends \Eloquent{

    public static $unguarded = true;
    public static function get_all()
    {
        return Post::all();
    }

    public static function get($id)
    {

        $post = Post::where('id', '=', $id)->first();

        return $post;
    }

    public static function add($data)
    {

        $post = Post::create([
            'title'=>$data['title'],
            'content'=>$data['content'],
            'author'=>'1',
        ]);

        return $post;
    }

}
