<?php namespace App\Http\Controllers;

use App\Models\Post;

use Validator;

class PostController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function getPost($id)
	{
        $id = (int)$id;

        $post = Post::get($id);

        return view('post.show-post')->with('post', $post);

	}

    public function addPost()
    {
        $post = Post::add($_POST);


        $rules = [
            'title'=>'required|min:8',
            'content'=>'required|min:8',
        ];

        $validator = Validator::make($_POST, $rules);

        $output = '';

        if ($validator->fails())
        {
            return view('errors.validation')->with('errors', $validator->messages()->toArray());
        }

        //return get_class($post);
        return redirect('/');
    }


}
