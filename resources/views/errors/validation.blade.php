@extends('template.template')

@section('template.content')

    @if(isset($errors))
        @foreach($errors as $error)
            {{ $error[0] }}
        @endforeach
    @else
        no
    @endif
<a href="{{ URL::previous() }}">back</a>
@stop