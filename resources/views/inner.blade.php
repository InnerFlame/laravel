@extends('template.template')

@section('template.content')

{!! Form::open() !!}

{!! Form::text('title', null, ['placeholder' => 'enter name','autocomplete' => 'off','class' => 'btn']) !!}

{!! Form::text('content', null, ['placeholder' => 'enter text','autocomplete' => 'off','class' => 'btn']) !!}

{!! Form::submit('Add') !!}

{!! Form::close() !!}

<!--<form action="/foo/bar" method="POST">-->
<!--    <input type="hidden" name="_method" value="PUT">-->
<!--    <input type="hidden" name="_token" value="--><?php //echo csrf_token(); ?><!--">-->
<!--</form>-->

    @if(isset($posts))
        @foreach($posts as $post)
            <section>
                <a href='/post/{{ $post->id }}'>{{ $post->title }}</a><br>
                <img style='float: left' src='{{ $post->image }}' alt='{{ $post->title }}'>
                <p style='float: left'>{{ $post->content }}</p><br>
                <p style='clear: both'>{{ $post->author }}</p>
            </section>
        @endforeach
    @else
        <p>no records</p>
    @endif
@stop