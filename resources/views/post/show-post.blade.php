@extends('template.template')

@section('template.content')

<section>
    <h2>{{ $post->title }}</h2>
    <img style='float: left' src='{{ $post->image }}' alt='{{ $post->title }}'>
    <p style='float: left'>{{ $post->content }}</p><br>
    <p style='clear: both'>{{ $post->author }}</p>
    <a href="/inner"> back </a>
</section>

@stop