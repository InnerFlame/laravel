<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Post;

class PostsSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{


        $faker = Faker\Factory::create();

        Post::truncate();// todo clear base

        for($i = 0;$i < 20;$i++)
        {
            Post::create([
                'title' => $faker->sentence(2),
                'content' => $faker->paragraph(20),
                'author' => $faker->name,
                'image'=> $faker->imageUrl($width = 150,$height = 150),
            ]);
        }

	}

}
